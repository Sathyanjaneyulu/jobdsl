freeStyleJob ('JenkinsTest') {
    description ('This is a seed job and used to create the jobs using dsl script')
    scm {
       git('https://gitlab.com/Sathyanjaneyulu/jobdsl.git', 'master')
         triggers {
             gitPush()
            publishers {
                archieveArtifacts('build/**/*.html')
            }
        }
     }
}
